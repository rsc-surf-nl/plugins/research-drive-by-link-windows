$Logfile = "C:\logs\rd-link.log"

Function LogWrite
{
  Param ([string]$logstring)
  '{0:u}: {1}' -f (Get-Date), $logstring | Out-File $Logfile -Append
}

Function Main { 
  LogWrite("Start researchdrive-by-link-windows")
  LogWrite("Get variables")

  $ProgressPreference = 'SilentlyContinue' # fast Invoke-WebRequest

  # Set-ItemProperty -Path HKLM:\Software\Policies\Microsoft\Windows\PowerShell -Name ExecutionPolicy -Value ByPass

  $rd_user = [System.Environment]::GetEnvironmentVariable('rd_by_link_user')
  $password =[System.Environment]::GetEnvironmentVariable('rd_by_link_pass') 
  $url = [System.Environment]::GetEnvironmentVariable('rd_by_link_url')
  $encrypt_secret_1 = [System.Environment]::GetEnvironmentVariable('rd_by_link_encrypt_secret_1')
  $encrypt_secret_2 = [System.Environment]::GetEnvironmentVariable('rd_by_link_encrypt_secret_2')
  $encrypt_filenames = [System.Environment]::GetEnvironmentVariable('rd_by_link_encrypt_filenames')
  $mountdir = [System.Environment]::GetEnvironmentVariable('rd_by_link_mountdir')

  # Boolean parameters:

  $rd_by_link_allow_all = [System.Environment]::GetEnvironmentVariable('rd_by_link_allow_all_users')
  $allow_all = [bool]::Parse($rd_by_link_allow_all)

  $rd_by_link_use_encryption = [System.Environment]::GetEnvironmentVariable('rd_by_link_use_encryption')
  $use_encryption = [bool]::Parse($rd_by_link_use_encryption)

  $rd_by_link_encrypt_directorynames = [System.Environment]::GetEnvironmentVariable('rd_by_link_encrypt_directorynames')
  $encrypt_directorynames = [bool]::Parse($rd_by_link_encrypt_directorynames)
  
  LogWrite("Install rclone")

  $rclone_exec_path="C:\rclone\rclone.exe"
  $rclone_conf_path="C:\rclone\rclone.conf"

  $urlRclone = "https://downloads.rclone.org/rclone-current-windows-amd64.zip"
  Invoke-WebRequest -Uri $urlRclone -OutFile "$env:TEMP\rclone-windows.zip"
  Expand-Archive "$env:TEMP\rclone-windows.zip" -DestinationPath "$env:TEMP\rclone-windows-unzip"
  New-Item -Path "c:\" -Name "rclone" -ItemType "directory"
  Get-ChildItem -Path "$env:TEMP\rclone-windows-unzip\*.exe" -Recurse | Move-Item -Destination $rclone_exec_path
  
  $ACL = Get-Acl -Path c:\rclone
  $AccessRule = New-Object System.Security.AccessControl.FileSystemAccessRule("Users", "FullControl", "ContainerInherit, ObjectInherit", "None", "Allow")
  $ACL.SetAccessRule($AccessRule)
  $ACL | Set-Acl -Path c:\rclone

  Remove-Item "$env:TEMP\rclone-windows.zip"
  Remove-Item "$env:TEMP\rclone-windows-unzip" -Recurse

  # Install WinFsp

  $winfsp_url="https://github.com/winfsp/winfsp/releases/download/v2.0/winfsp-2.0.23075.msi"
  $winfsp_msi_path="$env:TEMP\winfsp.msi"
  Invoke-WebRequest -Uri $winfsp_url -OutFile $winfsp_msi_path

  Start-Process msiexec.exe -Wait -ArgumentList @(
    "/I"
    "$winfsp_msi_path"
    "/quiet"
    "/norestart"
  )
    
  Remove-Item $winfsp_msi_path

  LogWrite("Create master rclone config file")

# DEBUGGING write params to file for manual experiments:
#   Set-Content -Path 'C:\rclone\params.txt' -Value @"
#   use_encryption: $use_encryption
#   encrypt_directorynames: $encrypt_directorynames
#   encrypt_filenames: $encrypt_filenames
#   rd_user: $rd_user
#   password: $password
#   url: $url
#   encrypt_secret_1: $encrypt_secret_1
#   encrypt_secret_2: $encrypt_secret_2
#   mountdir: $mountdir
# "@

  [System.Environment]::SetEnvironmentVariable('RCLONE_CONFIG', $rclone_conf_path)

  $crypt_config=""
  $remote_to_mount = "rdbylink"

  if ($use_encryption) {
    $crypt_config= @"
    [rdcrypt]
    type=crypt
    remote=rdbylink:
    filename_encryption=$encrypt_filenames
    directory_name_encryption=$encrypt_directorynames
    password=_to_be_obfuscated_1
    password2=_to_be_obfuscated_2
"@
    $remote_to_mount = "rdcrypt"
  }

  Set-Content -Path $rclone_conf_path -Value @"
  [rdbylink]
  type=webdav
  vendor=owncloud
  pass=_to_be_obfuscated_
  user=$rd_user
  url=$url
  $crypt_config
"@

Start-Process $rclone_exec_path -Wait -ArgumentList @(
    "config"
    "update"
    "rdbylink"
    "pass=$password"
)


if ($use_encryption) {
  Start-Process $rclone_exec_path -Wait -ArgumentList @(
    "config"
    "update"
    "rdcrypt"
    "password=$encrypt_secret_1"
    "password2=$encrypt_secret_2"
  )
}
  


  LogWrite("Task: at user login, mount $remote_to_mount to public dir")

# this works in the pwrsh window:
# .\rclone.exe mount rdbylink: C:\users\<user>\non-existing-dir 

# put mounting pwsh script in place to be executed by logon-task
  Set-Content -Path 'C:\rclone\mount4user.ps1' -Value @"
  C:\rclone\rclone.exe mount ${remote_to_mount}: C:\Users\Public\$mountdir
"@

  $tr = New-ScheduledTaskTrigger -AtLogOn
  # Make sure the task is run for any user who logs in
  $group_id = "INTERACTIVE"
  if (-not $allow_all) {
    $group_id = "BUILTIN\Administrators"
  }

  $pr = New-ScheduledTaskPrincipal -Groupid $group_id
  $act = New-ScheduledTaskAction -Execute "PowerShell.exe" -Argument "-WindowStyle Hidden C:\rclone\mount4user.ps1"
  Register-ScheduledTask -TaskName "Mount RD link for User" -Trigger $tr -Action $act -Principal $pr

  LogWrite("End researchdrive-by-link-windows") 
 }

 try {
    Main
}
catch {
    LogWrite("$_")
    Throw $_   
}  

